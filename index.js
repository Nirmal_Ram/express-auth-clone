var cookieParser = require('cookie-parser')
var csrf = require('csurf')
var bodyParser = require('body-parser')
var express = require('express')
const port = 4000
var jwt = require('jsonwebtoken');

// mock user 
var users ={
    "email":"fake@administrator.com",
    "user":"admin"
}


// setup route middlewares
var csrfProtection = csrf({ cookie: true })
var parseForm = bodyParser.urlencoded({ extended: false })
 
// setup express app
var app = express()
// parse cookies
// we need this because "cookie" is true in csrfProtection
app.use(cookieParser())
app.use(bodyParser.json())


// secret access token
const accessTokenSecret = 'VXpGacQd-zA-XiiNYnh-Mq0LB2d4qfAfQF8E';

// mock _csrf and sig
const csrfSecretJson = {
    "csrfSecret":"N1CIJZNOckfbu7gh8qt-Cwhm"
}

//middleware for generating XSRF-Token
function generateToken(req,res,next){
    app.set('token', req.csrfToken());
    res.cookie('XSRF-TOKEN',app.get("token"));
    next();
}

//middleware for generating Mock ssid and ssid.sig
function generateSSID(req,res,next){
    //const _csrf= req.headers['cookie']
    //console.log(req.headers.cookie['_csrf'])
    
    const accessToken = jwt.sign(csrfSecretJson,accessTokenSecret);
    const ssid = accessToken.split('.');
    res.cookie('ssid', ssid[1]);
    res.cookie('ssid.sig',ssid[2]);
    next();
}

//initial generation of XSRF token
app.get('/generate', csrfProtection,generateToken,generateSSID, function (req, res) {
    res.status(200).send("token generated . check cookies")
})

// verify XSRF Token from client to match to token server sends
function verifyCSRFToken(req,res,next) {
    if(req.headers["xsrf-token"] === app.get('token'))
        next()
    else
        return res.status(401).send('Unauthorised Access')
}
 
// POST request for login
app.post('/auth', parseForm, csrfProtection,verifyCSRFToken,generateToken, function (req, res, next) {
    const email = req.body.email;
    const user = req.body.user;
    //console.log(email + " -- "+ user + "--" + users.email + "--" + users.user )
    // authenticate the user input
    if(email === users.email && user === users.user){
        const updatedSSID = {
            "csrfSecret":csrfSecretJson.csrfSecret,
            "jwt":{
                "email": email,
                "user": user
            }
        }
        // generate new token for ssid and ssid.sig
        const newAccessToken = jwt.sign(updatedSSID,accessTokenSecret);
        const newSsid = newAccessToken.split('.');
        res.cookie('ssid', newSsid[1]);
        res.cookie('ssid.sig',newSsid[2]);
        res.status(200).send('You are authenticated')
    } 
    else{
        return res.status(401).send("Incorrect Crediatials")
    }

})

// protected GET request
app.get('/devices', csrfProtection,verifyCSRFToken,generateToken,function (req, res) {
    res.status(200).send("The list of devices")
})

// Logout 
app.get('/logout',csrfProtection,generateToken,generateSSID, function (req, res) {
    res.status(200).send("Logged Out")
})

app.listen(port,()=>{
    console.log("Server is Running on http://localhost:"+ port)
})